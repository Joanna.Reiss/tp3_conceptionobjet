using System;
using Xunit;
using DinoConsole;

namespace DinoTest
{
    public class UnitTest1
    {

        [Fact]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);

            Assert.Equal("Louis", louis.GetName());
            Assert.Equal("Stegosaurus", louis.GetSpecie());
            Assert.Equal(12, louis.GetAge());
        }

        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Assert.Equal("Grrr", louis.roar());
        }

        [Fact]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Assert.Equal("Je suis Louis le Stegosaurus, j'ai 12 ans.", louis.sayHello());
        }

        [Fact]
        public void TestHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "LochNessus", 500); // Me tapez pas svp
            Assert.Equal("Louis fait un calin a Nessie", louis.hug(nessie));
        }

        //Je ne sais pas comment effectuer les test pour vérifier la classe horde

    }
}
