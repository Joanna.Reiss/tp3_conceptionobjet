
using System;
using System.Collections.Generic;
using DinoConsole;

namespace DinoConsole
{
    public class Horde
    {
        private List<Dinosaur> troupeau;

        public Horde()
        {
            List<Dinosaur> troupeau = new List<Dinosaur>();
        }

        public void AddDinosaur(Dinosaur NewDinosaur)
        {
            this.troupeau.Add(NewDinosaur);

        }

        public void RemoveDinosaur(Dinosaur UnwantedDinosaur)
        {
            this.troupeau.Remove(UnwantedDinosaur);
        }

        public void Presentation()
        {
            foreach (Dinosaur Dino in troupeau)
            {
                Dino.sayHello();
            }
        }


    }
}