﻿using System;
using System.Collections.Generic;
using DinoConsole;

namespace DinoConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur matt = new Dinosaur("Matt", "Pedofilius", 57);
            Dinosaur ivi = new Dinosaur("ivi", "vivisaurus", 23);

            List<Dinosaur> dinosaurs = new List<Dinosaur>();

            dinosaurs.Add(louis); //Append dinosaur reference to end of list
            dinosaurs.Add(nessie);
            dinosaurs.Add(matt);
            dinosaurs.Add(ivi);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(String.Format("Je s appelle {0}", dino.GetName()));
            }

            dinosaurs.RemoveAt(1); //Remove dinosaur at index 1

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.GetName());
            }

            dinosaurs.Remove(louis);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.GetName());
            }

            Horde jurassik = new Horde();
            jurassik.AddDinosaur(louis);
            jurassik.AddDinosaur(nessie);
            jurassik.RemoveDinosaur(nessie);
        }
    }
}
