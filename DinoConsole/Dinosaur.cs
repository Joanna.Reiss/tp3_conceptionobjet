using System;

namespace DinoConsole
{
    public class Dinosaur
    {
        private string name;
        private string specie;
        private int age;


        public Dinosaur(string ArgumentName, string Argumentspecie, int ArgumentAge)
        {
            name = ArgumentName;
            specie = Argumentspecie;
            age = ArgumentAge;

        }

        public string sayHello()
        {
            return String.Format("Je suis {0} le {1}, j'ai {2} ans.", name, specie, age);
        }

        public string roar()
        {
            return "Grrr";
        }

        public string GetName()
        {
            return this.name;
        }

        public string GetSpecie()
        {
            return this.specie;
        }

        public int GetAge()
        {
            return this.age;
        }

        public void SetName(string ArgumentName)
        {

            this.name = ArgumentName;
        }

        public void SetSpecie(string Argumentspecie)
        {
            this.specie = Argumentspecie;
        }

        public void SetAge(int ArgumentAge)
        {
            this.age = ArgumentAge;
        }

        public string hug(Dinosaur DinoFriend)
        {
            string DinoFriendName = DinoFriend.GetName();
            return String.Format("{0} fait un calin a {1}", name, DinoFriendName);
        }
    }
}